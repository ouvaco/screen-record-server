import logging

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

#The background is set with 40 plus the number of the color, and the foreground with 30

#These are the sequences need to get colored ouput
RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[1;%dm"
BOLD_SEQ = "\033[1m"

COLORS = {
    'WARNING': YELLOW,
    'INFO': WHITE,
    'DEBUG': BLUE,
    'CRITICAL': YELLOW,
    'ERROR': RED
}

class ColoredFormatter(logging.Formatter):
    def __init__(self, msg, use_color = True):
        logging.Formatter.__init__(self, msg)
        self.use_color = use_color

    def format(self, record):
        levelname = record.levelname
        if self.use_color and levelname in COLORS:
            levelname_color = COLOR_SEQ % (30 + COLORS[levelname]) + levelname + RESET_SEQ
            record.levelname = levelname_color
        return logging.Formatter.format(self, record)

class Logger:
	def __init__(self, config):
		logger = logging.getLogger()
		logging.basicConfig(
			filename=config.get('logfile', 'dialog.log'),
			format='%(asctime)s - %(module)s.%(funcName)s - %(levelname)s - %(message)s',
			level=getattr(logging, config["file_level"], None))

		console_logger = logging.StreamHandler()
		formatter = ColoredFormatter(f"%(asctime)s - {COLOR_SEQ % (30 + GREEN)}%(module)s.%(funcName)s{RESET_SEQ} - %(levelname)s - %(message)s")
		console_logger.setFormatter(formatter)
		console_logger.setLevel(getattr(logging, config["console_level"], None))
		logger.addHandler(console_logger)
		self.logger = logger

		logging.info("Logger initiated, starting...")
