import argparse
import configparser
import os
import logging
import time
from logger import Logger
from flask import Flask
from flask import send_from_directory
import json
from pathlib import Path

from recorder import Recorder

def load_config(default_config_file):
    parser = argparse.ArgumentParser(description="screen-record-server")
    parser.add_argument('--config', '-c', type=str, default='')
    args = parser.parse_args()

    if not os.path.exists(default_config_file):
        logging.error(f"Default config file doesn't exist: {default_config_file}")
        exit(-1)

    config = configparser.ConfigParser(inline_comment_prefixes="#", allow_no_value=True)
    config.read("defaults.cfg")

    if not os.path.exists(args.config):
        logging.error(f"Config file doesn't exist: {args.config}, continuing with defaults")
    else:
        config.read(args.config)

    return config

CONFIG = load_config('defaults.cfg')
APP = Flask(__name__, static_url_path='')
PATH_VIDEO = None
PATH_THUMBNAIL = None
RECORDER = None

@APP.route('/videos')
def route_get_videos():
    return json.dumps({
        'result': 'success',
        'videos': [f.name.replace('.mp4', '') for f in Path(RECORDER.folder).rglob('*.mp4')]
    }).encode('ascii')

@APP.route('/recorder/start')
def route_get_recorder_start():
    global PATH_VIDEO
    PATH_VIDEO = RECORDER.start()
    videoname = PATH_VIDEO.split('/')[-1].split('.')[0]
    return json.dumps({
        'result': 'success',
        'video': videoname
    }).encode('ascii')

@APP.route('/recorder/stop')
def route_get_recorder_stop():
    global PATH_THUMBNAIL
    if not RECORDER.stop():
        return json.dumps({'result': 'error', 'message': 'Not recording.'}).encode('ascii')
    RECORDER.thumbnail(PATH_VIDEO)
    videoname = PATH_VIDEO.split('/')[-1].split('.')[0]
    return json.dumps({
        'result': 'success',
        'video': videoname  
    }).encode('ascii')

@APP.route('/video/<video>', methods=['GET'])
def route_get_video(video):
    path = RECORDER.folder + '/' + video + '.mp4'
    if os.path.isfile(path):
        return send_from_directory(RECORDER.folder, video + '.mp4')
    else:
        return json.dumps({'result': 'error', 'message': 'Video not found.'}).encode('ascii')

@APP.route('/video/<video>', methods=['DELETE'])
def route_delete_video(video):
    return json.dumps({'result': 'success'}).encode('ascii')

@APP.route('/thumb/<video>', methods=['GET'])
def route_get_thumb(video):
    path = RECORDER.folder + '/' + video + '.jpeg'
    if os.path.isfile(path):
        return send_from_directory(RECORDER.folder, video + '.jpeg')
    else:
        return json.dumps({'result': 'error', 'message': 'Thumb not found.'}).encode('ascii')

@APP.route('/recording/<filename>/shareemail', methods=['POST'])
def route_get_recording_shareemail(filename):
    return json.dumps({'result': 'error', 'message': 'Route not implemented.'}).encode('ascii')

@APP.route('/recording/<filename>/sharetext', methods=['POST'])
def route_get_recording_sharetext(filename):
    return json.dumps({'result': 'error', 'message': 'Route not implemented.'}).encode('ascii')

def main():
    global RECORDER
    Logger(CONFIG['logger'])
    logging.info('Starting...')

    RECORDER = Recorder()

    RECORDER.display_id = CONFIG.getint('recorder', 'display_id')
    RECORDER.capture_origin = (CONFIG.getint('recorder', 'capture_origin_x'), CONFIG.getint('recorder', 'capture_origin_y'))
    RECORDER.capture_size = (CONFIG.getint('recorder', 'capture_size_w'), CONFIG.getint('recorder', 'capture_size_h'))
    RECORDER.folder = CONFIG.get('recorder', 'folder')
    RECORDER.framerate = CONFIG.getint('recorder', 'framerate')
    RECORDER.size_output = (CONFIG.getint('recorder', 'size_output_w'), CONFIG.getint('recorder', 'size_output_h'))
    RECORDER.size_thumbnail = (CONFIG.getint('recorder', 'size_thumbnail_w'), CONFIG.getint('recorder', 'size_thumbnail_h'))
    RECORDER.thumbnail_time = CONFIG.getfloat('recorder', 'thumbnail_time')
    RECORDER.pixel_format = CONFIG.get('recorder', 'pixel_format')
    RECORDER.max_video_time = CONFIG.getfloat('recorder', 'max_video_time')

    APP.run(host=CONFIG.get('global', 'host'), port=CONFIG.getint('global', 'port'))

if __name__ == '__main__':
    main()