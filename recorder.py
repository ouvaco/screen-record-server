import ffmpeg
import subprocess
import logging
import datetime
import threading
import time

class Recorder:
    def __init__(self):
        self.process = None
        self.display_id = 0
        self.capture_origin = (0,0)
        self.capture_size = (3840, 2160)
        self.folder = 'videos'
        self.framerate = 25
        self.size_output = (320, -1)
        self.size_thumbnail = (320, -1)
        self.thumbnail_time = 0.1
        self.pixel_format = 'yuv420p'
        self.max_video_time = 300

    def start(self):
        outpath = f"{self.folder}/{datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}.mp4"
        logging.info(f'Started capturing desktop {self.capture_origin}:{self.capture_size} to file {outpath}')

        self.process = (ffmpeg
            .input(
                f':{self.display_id}.0+{self.capture_origin[0]},{self.capture_origin[1]}', 
                framerate=self.framerate, 
                video_size=self.capture_size, 
                f='x11grab')
            .filter('scale', self.size_output[0], self.size_output[1])
            .output(outpath, pix_fmt=self.pixel_format)
            .run_async(pipe_stdin=True))

        self.thread = threading.Thread(target=self.stop_after_delay)
        self.thread.daemon = True
        self.thread.start()

        return outpath

    def stop_after_delay(self):
        time.sleep(self.max_video_time)
        t = time.time()
        while self.process:
            dt = time.time() - t
            if dt > self.max_video_time:
                self.stop()
                logging.info(f"Stopped after recording timed out after {dt} secs.")
                break
            time.sleep(0.1)
        logging.info("Joined stop timeout thread.")

    def stop(self):
        if self.process != None:
            self.process.stdin.write(b'q')
            self.process.stdin.close()
            self.process.wait()
            self.process = None
            return True
        return False

    def thumbnail(self, path_video):
        path = path_video.replace('.mp4', '.jpeg')
        logging.info(f"Saving thumbnail to file {path}")
        (ffmpeg
            .input(path_video, ss=self.thumbnail_time)
            .filter('scale', self.size_thumbnail[0], self.size_thumbnail[1])
            .output(path, vframes=1)
            .run())
        return path
